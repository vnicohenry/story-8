from django.test import TestCase, Client
from django.urls import resolve

class LearnajaxUnitTest(TestCase):
    def test_status_url_exists(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_template_used(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'index.html')
# Create your tests here.
