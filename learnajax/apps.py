from django.apps import AppConfig


class LearnajaxConfig(AppConfig):
    name = 'learnajax'
